import Vue from "vue";
import Router from "vue-router";
import QuickCreditAndFraud from "./components/QuickCreditAndFraud.vue";
import SuccessMsgPage from "./components/SuccessMsgPage.vue";
import UserRegister from "./components/UserRegister.vue";
import Login from "./components/Login.vue";
import ChatBot from "./components/ChatBot.vue";
import UserHistory from "./components/UserHistory.vue";
import UserMangement from "./components/UserMangement.vue";
import GeneratePassword from "./components/GeneratePassword";
import TraceUser from "./components/TraceUser";
import BulkCsvFileUploadDownload from "./components/BulkCsvFileUploadDownload";
import BulkCsvDownload from "./components/BulkCsvDownload"
//import SideBarMenu from "./components/SideBarMenu"
/*import Verification from "./components/Verification"
import TermsAndCondition from "./components/TermsAndCondition"
import VerificationSearchResult from "./components/VerificationSearchResult"
import VehicleLogin from "./components/VehicleLogin"
//terms and conditions pages
import TransUnionAutoInfo from "./components/TermsAndConditionsPages/TransUnionAutoInfo"*/



Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    /* {
       path: "/",
       name: "creditandfraud-info",
       alias: "/creditandfraud-info",
       component: QuickCreditAndFraud,
      
     },*/
    {
      path: "/chatbot",
      name: "chatbot",
      alias: "/chatbot",
      component: ChatBot,

    },

    {
      path: "/creditandfraud-info",
      name: "creditandfraud-info",
      component: QuickCreditAndFraud
    },
    {
      path: "/success",
      name: "SuccessMsgPage",
      component: SuccessMsgPage
    },
    {
      path: "/userregister",
      name: "UserRegister",
      component: UserRegister
    },
    {
      path: "/",
      name: "Login",
      alias: "/Login",
      component: Login
    },
    {
      path: "/user-history",
      name: "UserHistory",
      component: UserHistory
    },
    {
      path: "/user-management",
      name: "UserMangement",
      component: UserMangement
    },

    {
      path: "/generate-password",
      name: "GeneratePassword",
      component: GeneratePassword
    },
    {
      path: "/traceuser-creditandfraud",
      name: "TraceUser",
      component: TraceUser
    },
    {
      path: "/bulkcsvfile-uploadanddownload",
      name: "BulkCsvFileUploadDownload",
      component:BulkCsvFileUploadDownload
    },
    {
      path: "/bulkcsvfile-download",
      name: "BulkCsvDownload",
      component:BulkCsvDownload
    },
    /*{
      path: "/sidebar-menu",
      name: "SideBarMenu",
      component: SideBarMenu

    },*/
    //verification website pages
    /*{
      path: "/verification",
      name: "Verification",
      component: Verification

    },
    {
      path: "/termsandconditions",
      name: "TermsAndCondition",
      component: TermsAndCondition

    },
    {
      path: "/verification-searchresult",
      name: "VerificationSearchResult",
      component: VerificationSearchResult
    },

    {
      path: "/vehicle-login",
      name: "VehicleLogin",
      component: VehicleLogin
    },

    //terms and conditions pages 

    {
      path: "/transUnion-autoinfo",
      name: "TransUnionAutoInfo",
      component: TransUnionAutoInfo
    },*/
    
    
  ]
});