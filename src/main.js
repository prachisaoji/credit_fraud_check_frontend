import Vue from 'vue'
import App from './App.vue'
import router from './router'
import BootstrapVue from 'bootstrap-vue/dist/bootstrap-vue.esm';
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import { BootstrapVueIcons } from 'bootstrap-vue'



import Vuelidate from 'vuelidate'
import VueCookies from 'vue-cookies'
import jQuery from "jquery";
global.jQuery=jQuery
global.$=jQuery
import locale from 'element-ui/lib/locale/lang/en';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI, { locale });

import { VuejsDatatableFactory } from 'vuejs-datatable';
Vue.use( VuejsDatatableFactory );

Vue.use(BootstrapVueIcons)


Vue.use(Vuelidate)
Vue.use(BootstrapVue)

Vue.use(VueCookies)
//base url
//Vue.prototype.$baseUrl = "http://localhost:8090/api";

Vue.prototype.$baseUrl = "http://ec2-65-0-40-228.ap-south-1.compute.amazonaws.com:8090/api";



Vue.config.productionTip = false

new Vue({
  router, 
  render: h => h(App),
}).$mount('#app')
